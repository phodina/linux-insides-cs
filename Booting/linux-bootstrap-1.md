Zaváděcí proces jádra. Část 1.
================================================================================

Od zavaděče k jádru
--------------------------------------------------------------------------------

Pokud jste si četli mé předchozí [články](https://0xax.github.io/categories/assembler/), potom jste jistě viděli, že jsem se jistý čas věnoval nízkoúrovňovému programování. Napsal jsem nějaké články o programování v jazyce symbolických adres pro `x86_64` Linux a ve stejný čas jsem se také ponořil do zdrojového kódu Linuxu.

Velmi mě zajímá jak nízkoúrovňové věci fungují, jak běží programy na mém počítači, jak jsou umístěny v paměti, jak jádro spravuje procesy a paměť, jak funguje síťový zásobník a další mnoho dalších věcí. Tudíž jsem se rozhodl napsat další řadu článků na o Linuxovém jádře na  **x86_64** architektuře.

Pro upřesnění, nejsem profesionální jaderný vývojář ani hacker. Je to jen mé hobby. Mám jen rád nízkoúrovňové věci a je pro mě zajímavé vědět, jak fungují. Pokud si všimnete něčeho matoucího nebo budete mít nějakou otázku/připomínku, dejte mě vědět na Twitteru [0xAX](https://twitter.com/0xAX), napište mi [email](anotherworldofworld@gmail.com) nebo vytvořte  [problém](https://github.com/0xAX/linux-insides/issues/new). Ocením to..

Všechny články jsou přístupné v repozitáři na [githubu](https://github.com/0xAX/linux-insides) a pokud najdete chybu v překladu nebo textu samotném, neváhejte poslat opravu.

*Poznámka: Toto není oficiální dokumentace, jen učení se a sdílení poznatků.*

**Požadované znalosti**

* Porozumění kódu v C 
* Porozumění kódu v jazyce symbolických adres ( syntaxe AT&T)

Nicméně pokud s těmito jazyky začínáte, nevadí, pokusím se vysvětlit jisté části v následujících článcích. Dobrá, došli jsme na konec jednoduchého úvodu a je čas se ponořit do Linuxového jádra a nízkoúrovňových věcí.

Začal jsem psát tuto knihu v době vydání Linuxového jádra `3.18` a od té doby se mnohé věci mohly změnit. Pokud se tak stane, pokusím se příhodně změnit články.

Magické tlačítko napájení, Co se stane poté?
--------------------------------------------------------------------------------

Ačkoliv toto je série článků o Linuxovém jádru, nezačneme ihned jaderným kódem - alespoň ne v tomto odstavci. Jakmile stisknete magické tlačítko napájení, laptopu nebo desktop počítač začne fungovat. Základní deska pošle signál [zdroji napájení](https://en.wikipedia.org/wiki/Power_supply), který dodá počítači nezbytné množství elektřiny. Poté co základní deska dostane signál [power good](https://en.wikipedia.org/wiki/Power_good_signal), pokusí se nastartovat CPU. CPU resetuje všechna zbylá data v registrech a nastaví v nich přednastavené hodnoty.

The [80386](https://en.wikipedia.org/wiki/Intel_80386) CPU a starší mají tyto přednastavené hodnoty v CPU registrech po resetu:

```
IP          0xfff0
CS selector 0xf000
CS base     0xffff0000
```

Procesor začíná v [real módu](https://en.wikipedia.org/wiki/Real_mode). Vraťme se trochu a podívejme se na  [segmenetaci paměti](https://en.wikipedia.org/wiki/Memory_segmentation) v tomto módu. Real mód je podporovaný na všech x86-kompatibilních procesorech, od [8086](https://en.wikipedia.org/wiki/Intel_8086) CPU až po moderní Intel 64-bitové CPU.  `8086` procesor má 20-bitovou adresovou sběrnici, tudíž může pracovat s adresou až `0-0xFFFFF` neboli `1 megabyte` adresovým prostorem. Procesor má ale jen `16-bit` registry, které mají maximální adresu `2^16 - 1` nebo `0xffff` (64 kilobytů).

[Pamťová segmentace](https://en.wikipedia.org/wiki/Memory_segmentation) se používá pro zpřístupnění celého adresního prostoru. Celá paměť je rozdělena na malé segmenty s pevnou velkostí o `65536` bytech (64 KB). Jelikož nemůžeme adresovat paměť nad `64 KB` s 16-bitovými registry, bylo zvoleno alternativní řešení.

Adresa se skládá ze 2 částí: výběrčí segmentu, který má základní adresu, a offset od této základní adresy. V reálném módu je asociovaná základní adresa výběrčí segmentu dána takto `Segment Selector * 16`. Pro fyzickou adresu v paměti potřebujeme vynásobit výběrčí segment 16 a přičíst offset:

```
PhysicalAddress = Segment Selector * 16 + Offset
```

Pokud například  `CS:IP` jsou `0x2000:0x0010`, potom odpovídající fyzická adresa je:

```python
>>> hex((0x2000 << 4) + 0x0010)
'0x20010'
```

Pokud ale vezmeme největší výběrčí segment a offset, `0xffff:0xffff`, tak dostaneme výslednou adresu:

```python
>>> hex((0xffff << 4) + 0xffff)
'0x10ffef'
```

Kde `65520` bytů je za prvním megabytem. Poněvadž je v real módu přístupný jen jeden megabyt paměti, adresa `0x10ffef` se změní na `0x00ffef` s [A20 řádkou](https://en.wikipedia.org/wiki/A20_line) vypnutou.

Dobrá, teď víme trochu teorie o real módu a paměťovém adresování v tomto módu. Vraťme se tedy k hodnotám registrů po resetu. 

`CS` registr obsahuje dvě části: viditelný výběrčí selektor a skrytou základní adresu. Zatímco je základní adresa normálně určena vynásobením výběrčího segmentu hodnotu 16, po restu je do CS registru načtena hodnota `0xf000` and základní adresa je nastavena na `0xffff0000`; procesor používá tuto speciální základní adresu dokud se hodnota `CS` nezmění.

Počáteční adresa je určena sečtením základní adresy s hodnotu v EIP registru:

```python
>>> 0xffff0000 + 0xfff0
'0xfffffff0'
```

Dostaneme adresu `0xfffffff0`, která je 16 byt§ pod 4GB. Toto místo nazýváme [Reset vektor](https://en.wikipedia.org/wiki/Reset_vector). Toto je paměťové místo na kterém CPU očekává, že najde první instrukci po resetu. Obsahuje instrukci pro [skok](https://en.wikipedia.org/wiki/JMP_%28x86_instruction%29) (`jmp`), která ukazuje na vstupní bod BIOSu. Pokud se například podíváme do zdrojového kódu [coreboot](https://www.coreboot.org/) (`src/cpu/x86/16bit/reset16.inc`), uvidíme následující:

```assembly
    .section ".reset", "ax", %progbits
    .code16
.globl	_start
_start:
    .byte  0xe9
    .int   _start16bit - ( . + 2 )
    ...
```

Zde vidíme [opkód](http://ref.x86asm.net/coder32.html#xE9) pro instrukci `jmp`, která je `0xe9`, a její cílová adresa je `_start16bit - ( . + 2)`.

Dále vidíme, že `reset` sekce je `16` bytů velká a je umístěna tak, aby začínala na adrese `0xfffffff0`  (`src/cpu/x86/16bit/reset16.ld`):

```
SECTIONS {
    /* Trigger an error if I have an unuseable start address */
    _bogus = ASSERT(_start16bit >= 0xffff0000, "_start16bit too low. Please report.");
    _ROMTOP = 0xfffffff0;
    . = _ROMTOP;
    .reset . : {
        *(.reset);
        . = 15;
        BYTE(0x00);
    }
}
```

Nyní se spustí BIOS. Po inicializaci a kontrole hardwaru musí BIOS najít zaváděcí zařízení. Pořadí zavaděče je v uloženo v konfiguraci BIOSu, určující z kterého zařízení se BIOS pokusí načíst zavaděč. Při načítání z disku se BIOS pokusí najít první sektor. Na hard discích s rozdělením typu MBR je zaváděcí sektor uložen v prvních `446` bytech prvního sektoru, kde velikost jednoho sektoru je `512` bytů. Poslední dva byty prvního sektoru jsou `0x55` a `0xaa` říkají BIOSu, že dané zařízení obsahuje zavaděč.

Například:

```assembly
;
; Note: this example is written in Intel Assembly syntax
;
[BITS 16]

boot:
    mov al, '!'
    mov ah, 0x0e
    mov bh, 0x00
    mov bl, 0x07

    int 0x10
    jmp $

times 510-($-$$) db 0

db 0x55
db 0xaa
```

Sestavte a spusťte:

```
nasm -f bin boot.nasm && qemu-system-x86_64 boot
```

Toto spustí [QEMU](http://qemu.org) s binárním souborem, který se použije jako obraz disku. Jelikož vygenerovaný binární soubor splňuje požadavky na zaváděcí sektor (začátek je nastaven na `0x7c00` a končí magickou sekvencí), QEMU bude považovat binární soubor za master boot record (MBR) obrazu disku.

Uvidíte:

![Simple bootloader which prints only `!`](http://oi60.tinypic.com/2qbwup0.jpg)

V tomto příkladu vidíte kód vykonávaný v `16-bit` real módu, který začíná na `0x7c00` v paměti. Po začátku zavolá přerušení [0x10](http://www.ctyme.com/intr/rb-0106.htm), které jen vypíše znak `!` ; zbylých `510` bytů je vyplněno nulami a končí dvěma magickými byty `0xaa` a `0x55`.

Můžeme se podívat na binární výpis pomocí nástroje `objdump` :

```
nasm -f bin boot.nasm
objdump -D -b binary -mi386 -Maddr16,data16,intel boot
```

Skutečný kód pokračuje zavaděčem a tabulkou oddílů namísto hromadou nul a vykřičníkem. :) Od tohoto místa předává BIOS kontrolu zavaděči.

**Poznámka**: Jak bylo vysvětleno výše, CPU je v real módu, výpočet fyzické adresy je následující:

```
PhysicalAddress = Segment Selector * 16 + Offset
```

Máme jen 16-bitové pracovní registry s maximální hodnotou `0xffff`, proto když vezmeme největší hodnoty dostaneme:

```python
>>> hex((0xffff * 16) + 0xffff)
'0x10ffef'
```

kde `0x10ffef` je rovno `1MB + 64KB - 16b`.  [8086](https://en.wikipedia.org/wiki/Intel_8086) procesor (který byl prvním procesorem s real módem), má ale jen 20-bitovou adresní sběrnici. `2^20 = 1048576` je 1MB, to znamená, že máme dostupný 1MB paměti.

Obecně to znamená, že v real módu je platná následující mapa paměti:

```
0x00000000 - 0x000003FF - Real Mode Interrupt Vector Table
0x00000400 - 0x000004FF - BIOS Data Area
0x00000500 - 0x00007BFF - Unused
0x00007C00 - 0x00007DFF - Our Bootloader
0x00007E00 - 0x0009FFFF - Unused
0x000A0000 - 0x000BFFFF - Video RAM (VRAM) Memory
0x000B0000 - 0x000B7777 - Monochrome Video Memory
0x000B8000 - 0x000BFFFF - Color Video Memory
0x000C0000 - 0x000C7FFF - Video ROM BIOS
0x000C8000 - 0x000EFFFF - BIOS Shadow Area
0x000F0000 - 0x000FFFFF - System BIOS
```

Na začátku článku jsem psal, že první instrukce je umístěna na adrese `0xFFFFFFF0`, která se mnohem vyšší než `0xFFFFF` (1MB). Jak může CPU přistoupit k této adrese v real módu? Odpověď je v dokumentaci [coreboot](https://www.coreboot.org/Developer_Manual/Memory_map):

```
0xFFFE_0000 - 0xFFFF_FFFF: 128 kilobyte ROM mapped into address space
```

Po spuštění není BIOS v RAM, ale v ROM.

Zavaděč
--------------------------------------------------------------------------------

Existuje mnoho zavaděčů, které dokáží načíst Linux, jako například [GRUB 2](https://www.gnu.org/software/grub/) a [syslinux](http://www.syslinux.org/wiki/index.php/The_Syslinux_Project). Linuxové jádro má [Zaváděcí protokol](https://github.com/torvalds/linux/blob/v4.16/Documentation/x86/boot.txt), který specifikuje požadavky na implementaci podpory v zavaděči. V příkladu popíši GRUB 2.

Počínaje předtím, BIOS nyní vybral zaváděcí zařízení a předal kontrolu zavaděči umístěném v zaváděcím sektoru, vykonávání instrukcí začne z [boot.img](http://git.savannah.gnu.org/gitweb/?p=grub.git;a=blob;f=grub-core/boot/i386/pc/boot.S;hb=HEAD). Jde o velmi jednoduchý kód vzhledem k dostupnému prostoru, proto obsahuje ukazatel, který se použije pro skok s obrazem jádra GRUB 2. Jádro obrazu začíná [diskboot.img](http://git.savannah.gnu.org/gitweb/?p=grub.git;a=blob;f=grub-core/boot/i386/pc/diskboot.S;hb=HEAD), které je obvykle umístěno v prvním sektoru v nepoužitém prostoru před prvním oddíle. Výše uvedený kód načte zbytek jádra obrazu, který obsahuje jádro GRUB 2 a ovladače souborových systémů do paměti. Po načtení zbytku jádra obrazu spustí funkci [grub_main](http://git.savannah.gnu.org/gitweb/?p=grub.git;a=blob;f=grub-core/kern/main.c).

Funkce `grub_main` inicializuje konzoli, získá základní adresu modulů, nastaví kořenové zařízení, načte/analyzuje konfigurační soubor, načte moduly, atd. Na konci funkce `grub_main` přepne grub do normálního módu. Funkce `grub_normal_execute` (v souboru `grub-core/normal/main.c`) dokončí přípravy a zobrazí menu pro výběr operačního systému. Když vybereme položku z menu, funkce `grub_menu_execute_entry` spustí grub `boot` příkaz a zavede operační systém.

Jak se můžeme dočíst v jaderném zaváděcím protokolu, zavaděč si musí přečíst a vyplnit jistá pole hlavičky jádra, která začíná offsetem `0x01f1` od inicializace kódu jádra. Můžete se podívat na [linkovací skript](https://github.com/torvalds/linux/blob/v4.16/arch/x86/boot/setup.ld) pro kontrolu hodnoty offsetu. Hlavička jádra [arch/x86/boot/header.S](https://github.com/torvalds/linux/blob/v4.16/arch/x86/boot/header.S) začíná zde:

```assembly
    .globl hdr
hdr:
    setup_sects: .byte 0
    root_flags:  .word ROOT_RDONLY
    syssize:     .long 0
    ram_size:    .word 0
    vid_mode:    .word SVGA_MODE
    root_dev:    .word 0
    boot_flag:   .word 0xAA55
```

Zavaděč musí vyplnit tyto a další hodnoty hlavičky (ty, které jsou označeny jako `write` v Linuxovém zaváděcím procesu, jako v této [ukázce](https://github.com/torvalds/linux/blob/v4.16/Documentation/x86/boot.txt#L354)) hodnotami, které jsou předány z příkazové řádky nebo spočítány během procesu zavádění. (Nebudeme popisovat všechny pole jaderné hlavičky, ale zaměříme se na to, k čemu je jádro používá; popis všech polí lze nalézt v  [zaváděcím protokolu](https://github.com/torvalds/linux/blob/v4.16/Documentation/x86/boot.txt#L156).)

Jak vidíte v jaderném zaváděcím protokolu, paměť bude mapována následovně:

```shell
         | Protected-mode kernel  |
100000   +------------------------+
         | I/O memory hole        |
0A0000   +------------------------+
         | Reserved for BIOS      | Leave as much as possible unused
         ~                        ~
         | Command line           | (Can also be below the X+10000 mark)
X+10000  +------------------------+
         | Stack/heap             | For use by the kernel real-mode code.
X+08000  +------------------------+
         | Kernel setup           | The kernel real-mode code.
         | Kernel boot sector     | The kernel legacy boot sector.
       X +------------------------+
         | Boot loader            | <- Boot sector entry point 0x7C00
001000   +------------------------+
         | Reserved for MBR/BIOS  |
000800   +------------------------+
         | Typically used by MBR  |
000600   +------------------------+
         | BIOS use only          |
000000   +------------------------+

```

Takže, když zavaděč předá kontrolu jádru, začne na adrese:

```
X + sizeof(KernelBootSector) + 1
```

kde `X` je adresa zaváděcího sektoru jádra, která se načítá. V mém případě je `X` `0x10000`, jak vidíte ve výpisu paměti:

![kernel first address](http://oi57.tinypic.com/16bkco2.jpg)

Zavaděč nyní načetl Linuxové jádro do paměti, vyplnil pole hlavičky a poté skočil na patřičnou adresu v paměti. Nyní se můžeme podívat na spouštěcí kód jádra.

Začátek spouštěcí etapy jádra
--------------------------------------------------------------------------------

Konečně jsme v jádře! Technicky ještě jádro ale neběží; nejprve musí jádro nastavit věci typu dekompresor, nějaké věci ohledně správy paměti a další. Poté co jsou všechny tyto věci hotové, spouštěč jádra dekomprimuje skutečné jádro a skočí do něho. Provedení nastavení je v souboru [arch/x86/boot/header.S](https://github.com/torvalds/linux/blob/v4.16/arch/x86/boot/header.S) pod symbolem [_start](https://github.com/torvalds/linux/blob/v4.16/arch/x86/boot/header.S#L292).

Může to na první pohled vypadat trochu podivně, ale prostě je třeba vykonat několik instrukc předtím. Kdysi dávno, Linuxové jádro mělo vlastní zavaděč. Teď, když spustíme například

```
qemu-system-x86_64 vmlinuz-3.18-generic
```

uvidíme:

![Try vmlinuz in qemu](http://oi60.tinypic.com/r02xkz.jpg)

Ve skutečnosti, soubor `header.S` začíná magickým číslem [MZ](https://en.wikipedia.org/wiki/DOS_MZ_executable) (obrázek uvedený výše), chybovou zprávou, která se zobrazí, a [PE](https://en.wikipedia.org/wiki/Portable_Executable) hlavičkou:

```assembly
#ifdef CONFIG_EFI_STUB
# "MZ", MS-DOS header
.byte 0x4d
.byte 0x5a
#endif
...
...
...
pe_header:
    .ascii "PE"
    .word 0
```

Potřebuje načíst operační systém s podporou [UEFI](https://en.wikipedia.org/wiki/Unified_Extensible_Firmware_Interface). Toto téma si necháme na nějaký pozdější článek.

Skutečný vstupní bod do jádra je:

```assembly
// header.S line 292
.globl _start
_start:
```

Zavaděč (grub2 a další) o tomto místu vědí (na offsetu `0x200` od `MZ`) a udělají skok přímo tam, navzdory tomu, že `header.S` začíná sekcí `.bstext`, která vypisuje chybové hlášení:

```
//
// arch/x86/boot/setup.ld
//
. = 0;                    // current position
.bstext : { *(.bstext) }  // put .bstext section to position 0
.bsdata : { *(.bsdata) }
```

Jaderný vstupní bod:

```assembly
    .globl _start
_start:
    .byte  0xeb
    .byte  start_of_setup-1f
1:
    //
    // rest of the header
    //
```

Zde vidíme opkód instrukce `jmp` (`0xeb`), který skočí na `start_of_setup-1f`. V notaci `Nf`, `2f`, to například znamená místní návěští `2:`; v našem případě je to návěští `1` , které je umístěno hned za skokem a obsahuje zbytek inicializační [hlavičky](https://github.com/torvalds/linux/blob/v4.16/Documentation/x86/boot.txt#L156). Hned za inicializační hlavičkou vidíme sekci `.entrytext`, která začíná na návěští `start_of_setup`.

Toto je první sekce kódu, která se skutečně vykoná (kromě předchozí instrukce skoku, samozřejmě). Poté co inicializační část jádra dostane kontrolu od zavaděče, je první `jmp` instrukce umístěna na offsetu `0x200` od začátku jádra v real módu, to znamená v prvních 512 bytech. Toto je možné spatřit jak u Linuxového zaváděcího protokolu, tak i ve zdrojovém kódu Grub 2:

```C
segment = grub_linux_real_target >> 4;
state.gs = state.fs = state.es = state.ds = state.ss = segment;
state.cs = segment + 0x20;
```

V mém případě je jádro načteno na adresu `0x10000`. To znamená, že segmentové registry budou obsahovat následující hodnoty po inicializaci jádra:

```
gs = fs = es = ds = ss = 0x10000
cs = 0x10200
```

Po skoku na `start_of_setup` musí jádro udělat následující:

* Zajisti, že všechny hodnoty všech segmentových registrů jsou stejné
* Natavit zásobník, je-li třeba
* Nastavit [bss](https://en.wikipedia.org/wiki/.bss)
* Skočit na C kód v [arch/x86/boot/main.c](https://github.com/torvalds/linux/blob/v4.16/arch/x86/boot/main.c)

Pojďme se podívat na implementaci.

Zarovnání segmentových registerů 
--------------------------------------------------------------------------------

Za prvé, jádro zaručuje že `ds` a `es` segmentové registry ukazují na stejnou adresu. Dále vyčistí směrový příznak pomocí `cld` instrukce:

```assembly
    movw    %ds, %ax
    movw    %ax, %es
    cld
```

Jak jsem napsal dříve `grub2`  načte standardně inicializační kód jádra na adresu `0x10000` a `cs` na `0x10200` jelikož vykonávání kódu nezačíná od začátku souboru, ale od skoku zde:

```assembly
_start:
    .byte 0xeb
    .byte start_of_setup-1f
```

který je na offsetu `512` bytů od [4d 5a](https://github.com/torvalds/linux/blob/v4.16/arch/x86/boot/header.S#L46). Dále také potřebujeme zarovnat `cs` z `0x10200` na `0x10000` stejně jako ostatní segmentové registry. Poté nastavíme zásobník:

```assembly
    pushw   %ds
    pushw   $6f
    lretw
```

tímto uložíme na zásobník hodnotu `ds` , dále pak adresu návěští [6](https://github.com/torvalds/linux/blob/v4.16/arch/x86/boot/header.S#L602) a vykoná instrukci `lretw`. Po zavolání instrukce `lretw`  se načte adresa návěští `6` do registru [čítače instrukcí](https://en.wikipedia.org/wiki/Program_counter) a načte do registru `cs` hodnotu registru `ds`. Poté budou mít registry `ds` a `cs` stejnou hodnotu.

Nastavení zásobníku
--------------------------------------------------------------------------------

Téměř všechen inicializační kód je příprava na prostředí jazyka C v real módu. Další [krok](https://github.com/torvalds/linux/blob/v4.16/arch/x86/boot/header.S#L575) je kontrola hodnoty `ss` registru a její případné nastavení:

```assembly
    movw    %ss, %dx
    cmpw    %ax, %dx
    movw    %sp, %dx
    je      2f
```

Uvažme 3 případy:

* `ss` má platnou hodnotu `0x1000` (stejně jako ostatní segmentové registry kromě `cs`)
* `ss` není platná a `CAN_USE_HEAP` příznak je nastaven (uvedeno níže)
* `ss` není platná a `CAN_USE_HEAP` příznak není nastaven (uvedeno níže)

Pojďme se podívat postupně na všechny 3 případy:

* `ss` má platnou adresu (`0x1000`). V tomto případě pokračujeme na návěští [2](https://github.com/torvalds/linux/blob/v4.16/arch/x86/boot/header.S#L589):

```assembly
2:  andw    $~3, %dx
    jnz     3f
    movw    $0xfffc, %dx
3:  movw    %ax, %ss
    movzwl  %dx, %esp
    sti
```

Zde vidíme zarovnání `dx` (které obsahuje hodnotu `sp` předanou zavaděčem) na `4` byty a kontrolu, zda není roven 0. Pro případ nuly nastavíme `0xfffc` (4 bytové zarovnání před maximální velikostí segmentu - 64 KB) v registru `dx`. V ostatních případech použijeme hodnotu `sp` předanou zavaděčem (0xf7f4 v mém případě). Poté předáme hodnotu `ax` do `ss`, která ukládá správný segment adresy `0x1000` a nastaví správně `sp`. Nyní máme správně nastavený zásobník:

![stack](http://oi58.tinypic.com/16iwcis.jpg)

* Druhý případ (`ss` != `ds`). Nejprve uložíme hodnotu [_end](https://github.com/torvalds/linux/blob/v4.16/arch/x86/boot/setup.ld) (adresa konce inicializačního kódu) do `dx` a zkontrolujeme pole `loadflags`  v hlavičce pomocí instrukce `testb` , zda můžeme použít haldu. [loadflags](https://github.com/torvalds/linux/blob/v4.16/arch/x86/boot/header.S#L320) je bitová maska zadefinovaná následovně:

```C
#define LOADED_HIGH     (1<<0)
#define QUIET_FLAG      (1<<5)
#define KEEP_SEGMENTS   (1<<6)
#define CAN_USE_HEAP    (1<<7)
```

 V zaváděcím protokolu se dočteme:

```
Field name: loadflags

  This field is a bitmask.

  Bit 7 (write): CAN_USE_HEAP
    Set this bit to 1 to indicate that the value entered in the
    heap_end_ptr is valid.  If this field is clear, some setup code
    functionality will be disabled.
```

Pokud je nastaven bit `CAN_USE_HEAP` , uložíme `heap_end_ptr` do `dx` (které ukazuje na `_end`) a přičteme k němu `STACK_SIZE` (minimální velikost zásobníku, `1024` bytů). Nedojde-li k přenosu u `dx` (nedojde k přenosu, `dx = _end + 1024`), provede se skok na návěští `2` (stejně jako v předchozím případě) a nastaví se správně zásobník.

![stack](http://oi62.tinypic.com/dr7b5w.jpg)

* Když není nastavený `CAN_USE_HEAP`, použijeme malý zásobník od `_end` do `_end + STACK_SIZE`:

![minimal stack](http://oi60.tinypic.com/28w051y.jpg)

BSS nastavení
--------------------------------------------------------------------------------

Poslední 2 kroky, které musíme udělat před skokem do kódu v C, je nastavit oblast [BSS](https://en.wikipedia.org/wiki/.bss) a zkontrolovat magický podpis. Nejdříve zkontroluje podpis:

```assembly
    cmpl    $0x5a5aaa55, setup_sig
    jne     setup_bad
```

Toto jednodušse porovná [setup_sig](https://github.com/torvalds/linux/blob/v4.16/arch/x86/boot/setup.ld) s magickou hodnotou `0x5a5aaa55`. Nejsou-li si rovny, je nahlášena kritická chyba.

Pokud se magické hodnoty shodují, pak víme, že máme správně nastavené segment registry a zásobník, tudíž stačí nastavit jen BSS sekci před skokem do kódu v jazyce C.

BSS sekce se používá na uložení staticky alokovaných neinicializovaných dat. Linux pečlivě zajistí, že tato paměťová oblast je nejdříve vynulována tímto kódem:

```assembly
    movw    $__bss_start, %di
    movw    $_end+3, %cx
    xorl    %eax, %eax
    subw    %di, %cx
    shrw    $2, %cx
    rep; stosl
```

Nejprve se [__bss_start](https://github.com/torvalds/linux/blob/v4.16/arch/x86/boot/setup.ld) adresa načte do registru `di`. Poté se `_end + 3` adresa (+3 - zarovnání na 4 byty) načte do registru `cx`. Registr `eax` je shozen (pomocí `xor` instrukce), a velikost bss sekce (`cx`-`di`) se uloží do `cx`. Poté se `cx` vydělí čtyřmi (velikost 'slova'), a `stosl` instrukce se opakovaně použije, ukládajíc hodnotu `eax` (nula) na adresu v registru `di`, automaticky se zvyšující o 4, pokračujíc dokud `cx` nedosáhne nuly. Výsledek kódu je, že se do paměti v rozsahu od  `__bss_start` do `_end` zapíší nuly:

![bss](http://oi59.tinypic.com/29m2eyr.jpg)

Skok do funkce main
--------------------------------------------------------------------------------

To je vše - máme zásobník a BSS, takže můžeme skočit do C funkce `main()`:

```assembly
    calll main
```

Funkce `main()` je umístěna v [arch/x86/boot/main.c](https://github.com/torvalds/linux/blob/v4.16/arch/x86/boot/main.c). V příští části se dočtete co funkce dělá.

Závěr
--------------------------------------------------------------------------------

Toto je konec první části o vnitřnostech Linuxového jádra. V případě dotazů nebo nápadů mě kontaktujte na Twitteru [0xAX](https://twitter.com/0xAX), napište mi [email](anotherworldofworld@gmail.com) nebo vytvořte [problém](https://github.com/0xAX/linux-internals/issues/new). V další části se podíváme na první kód v jazyce C, který se spustí v inicializační části jádra Linuxu, implementaci rutin jako `memset`, `memcpy`, `earlyprintk`, brzké implementace konzole a inicializace a mnoho dalšího.

Links
--------------------------------------------------------------------------------

  * [Intel 80386 programmer's reference manual 1986](http://css.csail.mit.edu/6.858/2014/readings/i386.pdf)
  * [Minimal Boot Loader for Intel® Architecture](https://www.cs.cmu.edu/~410/doc/minimal_boot.pdf)
  * [8086](https://en.wikipedia.org/wiki/Intel_8086)
  * [80386](https://en.wikipedia.org/wiki/Intel_80386)
  * [Reset vector](https://en.wikipedia.org/wiki/Reset_vector)
  * [Real mode](https://en.wikipedia.org/wiki/Real_mode)
  * [Linux kernel boot protocol](https://www.kernel.org/doc/Documentation/x86/boot.txt)
  * [coreboot developer manual](https://www.coreboot.org/Developer_Manual)
  * [Ralf Brown's Interrupt List](http://www.ctyme.com/intr/int.htm)
  * [Power supply](https://en.wikipedia.org/wiki/Power_supply)
  * [Power good signal](https://en.wikipedia.org/wiki/Power_good_signal)
