Zaváděcí proces jádra. Část 2.
================================================================================

První kroky v nastavení jádra
--------------------------------------------------------------------------------

V minulé [části](linux-bootstrap-1.md jsme se ponořili do linuxového jádra a viděli úvodní konfiguraci jádra. Skočili jsme u zavolání funkce `main` (první funkce napsaná v C) v [arch/x86/boot/main.c](https://github.com/torvalds/linux/blob/v4.16/arch/x86/boot/main.c).

V této části prozkoumáme:
* co je `protected` mód,
* přehod do něj,
* inicializace haldy a konzole,
* detekce paměti, validace CPU a inicializace klávesnice
* a mnoho dalšího.

Protected mód
--------------------------------------------------------------------------------

Předtím než se můžeme přepnout do Intel64 [Long módu](http://en.wikipedia.org/wiki/Long_mode), musí jádro přepnout CPU do protected módu.

Co je [protected mód](https://en.wikipedia.org/wiki/Protected_mode)? Protected mód byl přidán do vínku x86 architecture v roce 1982 a byl hlavním módem procesorů Intel od procesorů [80286](http://en.wikipedia.org/wiki/Intel_80286) po procesory Intel 64, které přinesly long mód.

Hlavní důvod odklonu od [Real módu](http://wiki.osdev.org/Real_Mode) je velmi limitovaný přístup k RAM. Pokud si vzpomínáte z minulé části, lze adresovat jen 2^20 bytes neboli 1 Megabyte, někdy dokonce jen 640 kilobytů.

Protected mód přinesl mnoho změn, ale tou hlavní je změna správy paměti. 20-bitový adresový přístup byl nahrazen 32-bitovou adresovou sběrnicí dovolující přístup k 4 Gigabytům paměti v porovnání s 1 Megabytem v real módu. Dále přibyla podpora [stránkování](http://en.wikipedia.org/wiki/Paging), o které se můžete dočíst v následujících sekcích.

Správa paměti je rozdělena v Protected módu do dvou téměř nezávislých částí:

* Segmentace
* Stránkování

Zde se budeme zabývat jen segmentací. Stránkování probereme poté.

Jak jste se mohli dočíst v předchozí části, adresa se v real módu skládá z:

* základní adresy segmentu
* Offsetu od základní adresy segmentu

Z těchto částí můžeme dostat fyzickou adresu následovně:

```
PhysicalAddress = Segment Selector * 16 + Offset
```

Paměťová segmentace byla v protected módu úplně předělána. Nejsou zde žádné 64 Kilobytové segmnety o pevné velikosti. Velikost a umístění pro každý segment je namísto toho posána v asociované datové struktuře nazvané _Segment Descriptor_. Segment deskriptory jsou uloženy v datové struktuře nazvané `Global Descriptor Table` (GDT).

GDT struktura je umístěna v paměti. Nemá žádnou pevnou adresu, její adresa je uložena ve speciálním  `GDTR`. Později v kódu uvidíme jak se GDT načte. Bude se jednat o operaci načtení do paměti podobnou této:

```assembly
lgdt gdt
```

kde `lgdt` instrukce načte základní adresu a velikost globální tabulky deskriptorů do `GDTR` registru. `GDTR` je  48-bitový register a skládá se ze dvou částí:

 * velikost (16-bit) globální tabulky deskriptorů;
 * adresa(32-bit) globální tabulky deskriptorů.

Jak jsem uvedl výše GDT obsahuje `segment descriptors` které popisují paměťové segmenty. Každý deskriptor má velikost 64-bitů. Obecné schéma deskriptoru je:

```
 63         56         51   48    45           39        32 
------------------------------------------------------------
|             | |B| |A|       | |   | |0|E|W|A|            |
| BASE 31:24  |G|/|L|V| LIMIT |P|DPL|S|  TYPE | BASE 23:16 |
|     |     | D   |     | L   | 19:16 |     |     |     | 1   | C   | R   | A   |     |
| --- |

 31                         16 15                         0 
------------------------------------------------------------
|                             |                            |
|        BASE 15:0            |       LIMIT 15:0           |
|     |     |
| --- |
```

Neobávejte se, vím že to vypadá složitě v porovnání s real módem, ale ve skutečnosti je to jednoduché. Například LIMIT 15:0 znamená, že bity 0-15 Limitu jsou umístěné na začátku Deskriptoru. Zbytek LIMITu 19:16 je umístěn mezi bity 48-51 Deskriptoru. Velikost Limitu je tedy 0-19 repektive 20-bitů. Pojďme se na něj podívat:

1. Limit[20-bits] je rozdělen mezi bity 0-15 a 48-51. Definuje `length_of_segment - 1`. Závisí na bitu `G`(Granularity).

  * je-li `G` (bit 55) shozen na 0 a limit segmentu je 0, pak je velikost segmentu 1 Byte
  * je-li `G` nastaven na 1 a limit segmentu je 0, pak je velikost segmentu 4096 Bytes
  * je-li `G` shozen na 0 a limit segmentu je 0xfffff, pak je velikost segmentu 1 Megabyte
  * je-li `G` nastaven na 1 a limit segmentu je 0xfffff, pak je velikost segmentu 4 Gigabytes

  To znamená, že
  * je-li G shozeno na 0, Limit je chápán v hodnotě 1 Bytu and maximální velkost segmentu může být 1 Megabyte.
  * je-li G nastaven na 1, Limit je chápán v hodnotě 4096 Bytes = 4 KBytes = 1 Page a maximální velkost segmentu může být 4 Gigabyty. Ve skutečnosti, když je G rovno 1, hodnota limitu je posunuta o 12 bitů doleva. Tudíž, 20 bitů + 12 bitů = 32 bitů a 2^32 = 4 Gigabytes.

2. Základ[32-bits] je rozdělen mezi bity 16-31, 32-39 a 56-63. Definuje fyzickou adresu počáteční adresy segmentu.

3. Typ/Atribut[5-bits] je reprezentovaný bity 40-44. Definuje typ segmentu a přístupu.
  * `S` příznak v bitu 44 specifikuje typ deskriptoru. Je-li `S` 0 pak se jedná o systémový segment, kdežto je-li `S` 1 pak jde o instrukční nebo datový segment (zásobníkové segmenty jsou datové segmenty s přístupem pro zápis/čtení).

Pro učení, zda jde o segment instrukční nebo datový, můžeme provnat `Ex` atribut (bit 43, oynačený jako 0 ve výše uvedeném diagramu). Je-li roven 0, pak jde o datový segment, jinak se jedná o instrukční segment.

Segment může být jeden z následujících typů:

```
| Type Field                  | Descriptor Type | Description                        |
| --------------------------- | --------------- | ---------------------------------- |
| Decimal                     |                 |
| 0    E    W   A             |                 |
| 0           0    0    0   0 | Data            | Read-Only                          |
| 1           0    0    0   1 | Data            | Read-Only, accessed                |
| 2           0    0    1   0 | Data            | Read/Write                         |
| 3           0    0    1   1 | Data            | Read/Write, accessed               |
| 4           0    1    0   0 | Data            | Read-Only, expand-down             |
| 5           0    1    0   1 | Data            | Read-Only, expand-down, accessed   |
| 6           0    1    1   0 | Data            | Read/Write, expand-down            |
| 7           0    1    1   1 | Data            | Read/Write, expand-down, accessed  |
| C    R   A                  |                 |
| 8           1    0    0   0 | Code            | Execute-Only                       |
| 9           1    0    0   1 | Code            | Execute-Only, accessed             |
| 10          1    0    1   0 | Code            | Execute/Read                       |
| 11          1    0    1   1 | Code            | Execute/Read, accessed             |
| 12          1    1    0   0 | Code            | Execute-Only, conforming           |
| 14          1    1    0   1 | Code            | Execute-Only, conforming, accessed |
| 13          1    1    1   0 | Code            | Execute/Read, conforming           |
| 15          1    1    1   1 | Code            | Execute/Read, conforming, accessed |
```

Jak vidíme první bit(bit 43) je `0` pro a _datový_ segment a `1` pro _instrukční_ segment. Následující tři bity (40, 41, 42) jsou buď `EWA`(*E*xpansion *W*ritable *A*ccessible) nebo CRA(*C*onforming *R*eadable *A*ccessible).
  * je-li E(bit 42) shozen na 0, expanduj nahoru, jinak expanduj dolů. Zde se dočtete [více](http://www.sudleyplace.com/dpmione/expanddown.html).
  * je-li W(bit 41)(pro datové Segmenty) nastaven na 1, přístup pro zápis je povolen, a je-li shozen na 0, segment je jen pro čtení. Pro datové segmenty je vždy přístup pro čtení povolen.
  * A(bit 40) ovládá zda procesor může přistupovat k segmentu.
  * C(bit 43) je bit přizpůsobení (pro výběr kódu). Je-li C 1, segment kódu může být vykonán z nižší privilegované úrovně (např. uživatelské). Je-li C 0, pak může být vykonán jen ze stejné privilegované úrovně.
  * R(bit 41) ovládá přístup pro čtení u instrukčního segmentu; když je 1, lze ze segmentu číst. Přístup pro zápis není nikdy povolen pro instrukční segmenty.

4. DPL[2-bity] (Descriptor Privilege Level) se skádá z bitů 45-46. Definuje úroveň privilegií pro segment. Může být v rozmezí 0-3, kde 0 odpovídá nejvíce privilegované úrovni.

5. P příznak(bit 47) ukazuje, zda je segment v paměti či ne. Je-li P 0, pak je gement označen jako _neplatný_ a procesor odmítne číst z tohoto segmentu.

6. AVL příznak(bit 52) - Dostupný a rezervovaný. V Linuxu ignorován.

7. L příznak(bit 53) ukazuje, zda segment kódu obsahuje 64-bitový kód. Je-li nastaven, potom se tento instrukční segment vykoná v 64-bitovém módu.

8. D/B příznak(bit 54)  (Default/Big flag) představuje velikost operandu. Je-li nastaven pak je velikost operandu 32 bitů. Jinak je 16 bitů.

Segmentové registery obsahují výběrčí segmentu jako v real módu. Ačkoliv v protected módu se s výběrčím kódu pracuje jinak. Každý deskriptor segmentu má asociovaný selektor segmentu, kterým je 16-bitová struktura:

```
 15             3 2  1     0
-----------------------------
| Index | TI  | RPL |
| ----- |
```

Kde,
* **Index** ukládá číslo indexu deskriptoru v GDT.
* **TI**(Table Indicator) ukazuje kde hledat deskriptor. Je-li 0 pak je deskriptor hledán v Global Descriptor Table(GDT). Jinak se hledá v Local Descriptor Table(LDT).
* **RPL** obsahuje úroveň privilegií žadatele.

Každý segment má viditelnou a skrytou část.
* Viditelná - Zde je uložen výběrčí segmentu.
* Skrytá -  Deskriptor segmentu (který obsahuje základ, limit, atributy & příznaky) je uložen zde.

Následující kroky popisují určení fyzické adresy v protected módu:

* Výběrčí selektor musí být načten do jednoho ze segmentových registrů.
* CPU se pokusí najít deskriptor segmentu na ofsetu `GDT address + Index` od výběrčího apoté načte deskriptor do _skryté_ části segmentového registru.
* Je-li zakázáno stránkování, je lineární adresa segmentu, nebo její fyzická adresa, dána následují formulí: základní adresa (nalezena v deskriptoru v předchozím kroku) + Offset.

Znázorněno to bude vypandat následovně:

![linear address](http://oi62.tinypic.com/2yo369v.jpg)

Mechanizmus přepnutí z real-módu do protected módu je následující:

* Zakázat přerušení
* Popsat a načíst GDT pomocí `lgdt` instrukce
* Nastavit PE (Protection Enable) bit v CR0 (Control Register 0)
* Skok do kódu v protected módu

Úplné přepnutí Linuxového jádra do protected-módu uvidíme v následující části, ale předtím ještě musíme udělat pár příprav.

Pojďme se podívat na [arch/x86/boot/main.c](https://github.com/torvalds/linux/blob/v4.16/arch/x86/boot/main.c). Zde jsou funkce pro inicializaci klávesnice, haldy ...

Kopírování zaváděcích parametrů do "zeropage"
--------------------------------------------------------------------------------

Odpíchneme se od `main` funce v "main.c". První volanou funcí v `main` je [`copy_boot_params(void)`](https://github.com/torvalds/linux/blob/v4.16/arch/x86/boot/main.c). Zkopíruje úvodní hlavičku jádra do patřičného položky struktury `boot_params`, která je definována v [arch/x86/include/uapi/asm/bootparam.h](https://github.com/torvalds/linux/blob/v4.16/arch/x86/include/uapi/asm/bootparam.h).

Struktura`boot_params` obsahuje položku `struct setup_header hdr`. Tato struktura obsahuje ty samé položky jako definované v  [zaváděcím protokolu linuxu](https://www.kernel.org/doc/Documentation/x86/boot.txt) a je vyplněna zavaděčem a částečně také při kompilaci jádra. `copy_boot_params` dělá dvě věci:

1. Kopíruje `hdr` z [header.S](https://github.com/torvalds/linux/blob/v4.16/arch/x86/boot/header.S#L280) do `setup_header` položky v `boot_params` struktuře.

2. Aktualizuje ukazatel na jadernou příkazovou řádku, pokud se jádro načetlo starým protokolem příkazové řádky.

Ke kopírování `hdr` se použije `memcpy` funkce, která je definována v [copy.S](https://github.com/torvalds/linux/blob/v4.16/arch/x86/boot/copy.S). Podívejme se na ní:

```assembly
GLOBAL(memcpy)
    pushw   %si
    pushw   %di
    movw    %ax, %di
    movw    %dx, %si
    pushw   %cx
    shrw    $2, %cx
    rep; movsl
    popw    %cx
    andw    $3, %cx
    rep; movsb
    popw    %di
    popw    %si
    retl
ENDPROC(memcpy)
```

Ano, přesunuli jsme se do kódu v C a hned zpátky do assembleru :) Zaprvé vidíme, že `memcpy` a ostatní rutiny, které jsou zde definovány, začínají a končí dvěma makry: `GLOBAL` a `ENDPROC`. `GLOBAL` popsán v [arch/x86/include/asm/linkage.h](https://github.com/torvalds/linux/blob/v4.16/arch/x86/include/asm/linkage.h), kde je definována `globl` direktiva a její návěští. `ENDPROC` popsán v  [include/linux/linkage.h](https://github.com/torvalds/linux/blob/v4.16/include/linux/linkage.h) a značí symbol `name` jako název funkce a končí velikostí symbolu `name`.

Implementace `memcpy` je jednoduchá. Nejdříveuloží na zásobník registry `si` a `di` , jelikož se jejich hodnoty budou měnit během volání `memcpy`. Proměnná `REALMODE_CFLAGS` je během sestavování jádra v `arch/x86/Makefile` nastavena na `-mregparm=3` , tudíž funkce dostanou první tři parametry přes registry `ax`, `dx` a `cx` registry.  Volání `memcpy` vypadá následovně:

```c
memcpy(&boot_params.hdr, &hdr, sizeof hdr);
```

Tudíž,
* `ax` bude obsahovat adresu `boot_params.hdr`
* `dx` bude obsahovat adresu `hdr`
* `cx` bude obsahovat velikost `hdr` v bytech.

`memcpy` vloží adresu `boot_params.hdr` do `di` a uloží `cx` na zásobník.Poté posune jeho hodnotu vpravo dvakrát (děleno 4) a zkopíruje 4 byty z adresy `si` do adresy v `di`. Následně obnovíme velikost `hdr`, zarovnáme je na 4 byty a zbytek zkopírujeme z adresy `si` na adresu `di` byt po bytu (je-li jich více). Nyní je hodnota `si` a `di` obnovena zpět ze zásobníku a kopírovací operace je dokončena..

Inicializace konzole
--------------------------------------------------------------------------------

Poté co je `hdr` zkopírováno do `boot_params.hdr` je dalším krokem inicializace konzole pomocí  `console_init` funkce, definované v [arch/x86/boot/early_serial_console.c](https://github.com/torvalds/linux/blob/v4.16/arch/x86/boot/early_serial_console.c).

Ta se pokusí najít volbu `earlyprintk` v příkazové řádce a pokud ji najde, tak si přečte adresu portu a přenosovou rychlost sériového portu, který inicializuje. Hodnota volby `earlyprintk` může být následující:

* serial,0x3f8,115200
* serial,ttyS0,115200
* ttyS0,115200

Poté co se sériový port inicializuje můžeme vidět první výstup:

```C
if (cmdline_find_option_bool("debug"))
    puts("early console in setup code\n");
```

Definice funkce `puts` je v [tty.c](https://github.com/torvalds/linux/blob/v4.16/arch/x86/boot/tty.c). Funkce vypisuje ve smyčce znak po znaku pomocí funkce `putchar`. Podívejme se na implementaci `putchar`:

```C
void __attribute__((section(".inittext"))) putchar(int ch)
{
    if (ch == '\n')
        putchar('\r');

    bios_putchar(ch);

    if (early_serial_base != 0)
        serial_putchar(ch);
}
```

`__attribute__((section(".inittext")))` znamená, že tento kód bude v sekci `.inittext` . Můžeme ji najít v llinker skriptu [setup.ld](https://github.com/torvalds/linux/blob/v4.16/arch/x86/boot/setup.ld).

`putchar` nejprve ověří, zda nejde o symbol `\n` a pokud jej nalezne, vytiskne nejdříve `\r`. Poté vytiskne znak na VGA obrazovku zavoláním přerušení `0x10` BIOSu:

```C
static void __attribute__((section(".inittext"))) bios_putchar(int ch)
{
    struct biosregs ireg;

    initregs(&ireg);
    ireg.bx = 0x0007;
    ireg.cx = 0x0001;
    ireg.ah = 0x0e;
    ireg.al = ch;
    intcall(0x10, &ireg, NULL);
}
```

Zde `initregs` obdrží `biosregs` strukturu, kterou nejprve vyplní nulami pomocí funkce `memset` function a následně ji nastaví dle hodnot registrů.

```C
    memset(reg, 0, sizeof *reg);
    reg->eflags |= X86_EFLAGS_CF;
    reg->ds = ds();
    reg->es = ds();
    reg->fs = fs();
    reg->gs = gs();
```

Podívejme se na implementaci [memset](https://github.com/torvalds/linux/blob/v4.16/arch/x86/boot/copy.S#L36):

```assembly
GLOBAL(memset)
    pushw   %di
    movw    %ax, %di
    movzbl  %dl, %eax
    imull   $0x01010101,%eax
    pushw   %cx
    shrw    $2, %cx
    rep; stosl
    popw    %cx
    andw    $3, %cx
    rep; stosb
    popw    %di
    retl
ENDPROC(memset)
```

Používá se zde stejná konvence jako u funkce `memcpy`, která znamená, že funkce dostane parametry z registrů `ax`, `dx` and `cx`.

Implementace `memset` je podovná jako u memcpy. Uloží `di` registr na zásobník a uloží hodnotu `ax` , která obsahuje adresu struktury `biosregs`, do `di` . Dále instrukce `movzbl`  zkopíruje hodnotu spodních 2 bytů  `dl` do `eax` registru. Zbývající horní dva byty registru `eax` jsou vyplněny nulami.

Další instrukce vynásobí `eax` hodnotou `0x01010101`. To je třeba jelikož `memset` bude kopírovat najedou 4 byty. Například, pokud potřebujeme vyplnit strukturu o velikosti 4 bytů hodnotou `0x7` pomocí memset, `eax`bude obsahovat `0x00000007`. Tudíž pokud vynásobíme `eax` hodnotou `0x01010101`, dostaneme `0x07070707`, jehož 4 byty můžeme překopírot do struktury. `memset` používá `rep; stosl` instrukce pro kopírování `eax` do `es:di`.

Zbytek `memset` funkce provádí téměř to samé jako `memcpy`.

Poté co je `biosregs` struktura naplněna pomocí `memset`, `bios_putchar` zavolá přerušení [0x10](http://www.ctyme.com/intr/rb-0106.htm) vytiskne znak. Poté zkontroluje, zda je sériový port inicializovaný či ne a vypíše znak tam pomocí [serial_putchar](https://github.com/torvalds/linux/blob/v4.16/arch/x86/boot/tty.c) a instrukcí `inb/outb` , pokud je inicializovaný.

Inicializace haldy
--------------------------------------------------------------------------------

Poté co je zásobník a bss sekce připravena v [header.S](https://github.com/torvalds/linux/blob/v4.16/arch/x86/boot/header.S) ([předchozí část](linux-bootstrap-1.md)), potřebuje jádro inicializovat [haldu](https://github.com/torvalds/linux/blob/v4.16/arch/x86/boot/main.c) pomocí funkce [`init_heap`](https://github.com/torvalds/linux/blob/v4.16/arch/x86/boot/main.c).

Funkce `init_heap` nejprve zkontroluje příznak [`CAN_USE_HEAP`](https://github.com/torvalds/linux/blob/v4.16/arch/x86/include/uapi/asm/bootparam.h#L24) ve struktuře [`loadflags`](https://github.com/torvalds/linux/blob/v4.16/arch/x86/boot/header.S#L320) v zavaděči jaderné hlavičky a spočte konec zásobníku, pokud je příznak nastaven:

```C
    char *stack_end;

    if (boot_params.hdr.loadflags & CAN_USE_HEAP) {
        asm("leal %P1(%%esp),%0"
            : "=r" (stack_end) : "i" (-STACK_SIZE));
```

jinými slovy `stack_end = esp - STACK_SIZE`.

Poté je tu výpočet `heap_end`:

```C
     heap_end = (char *)((size_t)boot_params.hdr.heap_end_ptr + 0x200);
```

který se rovná `heap_end_ptr` nebo `_end` + `512` (`0x200h`). Poslední kontrolou je zda `heap_end` je větší než `stack_end`. Je tomu tak, pak je `stack_end` přiřazen do `heap_end` , aby se rovnaly.

Nyní je halda inicializována a my můžeme použít metodu `GET_HEAP`. Uvidíme, kde a jak se používá a jak je implementována.

CPU validation
--------------------------------------------------------------------------------

Dalším krokem je validace CPU pomocí funkce `validate_cpu` z [arch/x86/boot/cpu.c](https://github.com/torvalds/linux/blob/v4.16/arch/x86/boot/cpu.c).

Volané funkci [`check_cpu`](https://github.com/torvalds/linux/blob/v4.16/arch/x86/boot/cpucheck.c) je předána současná a požadovaná CPU úroveň, proto, aby jádro bylo spuštěno na správné úrovni.

```C
check_cpu(&cpu_level, &req_level, &err_flags);
if (cpu_level < req_level) {
    ...
    return -1;
}
```

`check_cpu` funkce zkontroluje příznaky CPU, přítomnost [long módu](http://en.wikipedia.org/wiki/Long_mode) v případě x86_64(64-bit) CPU, zkontroluje výrobce procesoru a zahájí přípravy pro různé přípravy jako vypnutí SSE+SSE2 pro AMD, pokud chybí, ...

V dalším kroku vidíme volání funkce `set_bios_mode` poté co úvodní kód usoudí, že CPU vhodné. Tato funkce je definována jen pro `x86_64` mód:

```C
static void set_bios_mode(void)
{
#ifdef CONFIG_X86_64
	struct biosregs ireg;

	initregs(&ireg);
	ireg.ax = 0xec00;
	ireg.bx = 2;
	intcall(0x15, &ireg, NULL);
#endif
}
```

Funkce `set_bios_mode` zavolá `0x15` přerušení BIOSu, které oznámí BIOSu, že se použije [long mód](https://en.wikipedia.org/wiki/Long_mode) (je-li `bx == 2`).

Detekce paměti
--------------------------------------------------------------------------------

Dalším krokem je detekce paměti pomocí funkce `detect_memory`. `detect_memory` poskytuje mapu dostupn0 paměti pro CPU. Používá rozdílné programovací rozhraní pro detekci paměti jako `0xe820`, `0xe801` a `0x88`. Zde uvidíme jen implementaci rozhraní **0xE820**.

Podívejme se na implemnetaci funkce `detect_memory_e820` z [arch/x86/boot/memory.c](https://github.com/torvalds/linux/blob/v4.16/arch/x86/boot/memory.c). Nejdříve funkce  `detect_memory_e820` inicializuje strukturu `biosregs` jak jsme viděli výš a vyplní registry speciálními hodnotami pro `0xe820`:

```assembly
    initregs(&ireg);
    ireg.ax  = 0xe820;
    ireg.cx  = sizeof buf;
    ireg.edx = SMAP;
    ireg.di  = (size_t)&buf;
```

* `ax` obshauje číslo funkce( v našem případě 0xe820)
* `cx` obsahuje velikost bufferu, který bude obsahovat data o paměti
* `edx` musí obsahovat `SMAP` magické číslo
* `es:di` msí obsahovat adresu bufferu, který bude obsahovat paměťová data
* `ebx` musí být nula.

Dále smyčka posbírá data o paměti. Začne voláním `0x15` přerušení BIOSu, který zapíše jeden řádek z alované adresní tabulky. Pro další řádek musíme zavolat přeušení znova (v dalším cyklu smyčky). Předtím ale musí `ebx` obsahovat hosntou vrácenou předtím:

```C
    intcall(0x15, &ireg, &oreg);
    ireg.ebx = oreg.ebx;
```

Nakonec tato funkce posbírá data z tabulky alokovaných adres a zapíše je do pole `e820_entry`:

* začátek paměťového segmentu
* velikost paměťového segmentu
* typ pmaěťového segmentu (zda je daný segment použitelný či rezervovaný)

Zde vidíte výstup `dmesg`:

```
[    0.000000] e820: BIOS-provided physical RAM map:
[    0.000000] BIOS-e820: [mem 0x0000000000000000-0x000000000009fbff] usable
[    0.000000] BIOS-e820: [mem 0x000000000009fc00-0x000000000009ffff] reserved
[    0.000000] BIOS-e820: [mem 0x00000000000f0000-0x00000000000fffff] reserved
[    0.000000] BIOS-e820: [mem 0x0000000000100000-0x000000003ffdffff] usable
[    0.000000] BIOS-e820: [mem 0x000000003ffe0000-0x000000003fffffff] reserved
[    0.000000] BIOS-e820: [mem 0x00000000fffc0000-0x00000000ffffffff] reserved
```

Inicializace klávesnice
--------------------------------------------------------------------------------

Dalším krokem je inicializace klávesnice voláním funkce [`keyboard_init`](https://github.com/torvalds/linux/blob/v4.16/arch/x86/boot/main.c). Funkce `keyboard_init` inicializuje registry pomocí funkce `initregs`. Poté zavolá přerušení [0x16](http://www.ctyme.com/intr/rb-1756.htm) s dotazem na stav klávesnice.

```c
    initregs(&ireg);
    ireg.ah = 0x02;     /* Get keyboard status */
    intcall(0x16, &ireg, &oreg);
    boot_params.kbd_status = oreg.al;
```

Poté zavolá znovu přerušení [0x16](http://www.ctyme.com/intr/rb-1757.htm), aby nastavil rychlost opakování a zpoždění.

```c
    ireg.ax = 0x0305;   /* Set keyboard repeat rate */
    intcall(0x16, &ireg, NULL);
```

Dotazování
--------------------------------------------------------------------------------

Následujích pár kroků je řada dotazů na rozličné parametry. Momentálně je nebudeme rozebírat, ale později se k nim vrátíme. Zde jsou ve stručnosti dané funkce:

Prvním krokem je získání informace o [Intel SpeedStep](http://en.wikipedia.org/wiki/SpeedStep) voláním funkce `query_ist`. Ta zkontroluje CPU úroveň a pokud je platná, tak zavolá přerušení `0x15` pro získání infromace, kterou následně uloží do `boot_params`.

Dále dunkce [query_apm_bios](https://github.com/torvalds/linux/blob/v4.16/arch/x86/boot/apm.c#L21) získá informaci o [Advanced Power Management](http://en.wikipedia.org/wiki/Advanced_Power_Management) z BIOS. `query_apm_bios` volá také přerušení `0x15` BIOSu, ale s `ah` = `0x53` pro kontrolu `APM`. Po skončení přerušení `0x15` zkontroluje funkce `query_apm_bios` `PM` podpis (musí být `0x504d`), carry příznak (musí být 0 je-li `APM` podporováno) a hodnotu `cx` registru (je-li 0x02, je podporováno rozhraní v protected módu).

Dále opět zavolá přerušení `0x15`, ale s `ax = 0x5304` pro odpojení `APM` rozhraní a připojení rozhraní 32-bitového protected módu. Nakonec vyplní položku `boot_params.apm_bios_info` hodnotami obdrženými od BIOSu.

Funkce `query_apm_bios`  se vykoná jen, je-li nastaven příznak `CONFIG_APM` nebo `CONFIG_APM_MODULE` během kompilace v konfiguračním souboru:

```C
#if defined(CONFIG_APM) || defined(CONFIG_APM_MODULE)
    query_apm_bios();
#endif
```

Poslení je funkce [`query_edd`](https://github.com/torvalds/linux/blob/v4.16/arch/x86/boot/edd.c#L122), která se dotáže BIOSu na `Enhanced Disk Drive`. Pojďme se podívat na implementaci `query_edd`.

Nejříve přečte volbu [edd](https://github.com/torvalds/linux/blob/v4.16/Documentation/admin-guide/kernel-parameters.rst) z příkazové řádky jádra a pokud je nastavena na `off`, potom se `query_edd` jen vrátí.

Je-li EDD povoleno, pak `query_edd` projde hard-diksy, které BIOS podporuje a dotáže se na EDD informaci v následující smyčce:

```C
for (devno = 0x80; devno < 0x80+EDD_MBR_SIG_MAX; devno++) {
    if (!get_edd_info(devno, &ei) && boot_params.eddbuf_entries < EDDMAXNR) {
        memcpy(edp, &ei, sizeof ei);
        edp++;
        boot_params.eddbuf_entries++;
    }
    ...
    ...
    ...
    }
```

kde `0x80` je první hard drive a hodnota makra `EDD_MBR_SIG_MAX` 16. Posbírá data do pole struktury [edd_info](https://github.com/torvalds/linux/blob/v4.16/include/uapi/linux/edd.h). `get_edd_info` zkontroluje, zda je EDD přítomné voláním přerušení `0x13` s hodnotu `ah` nastavenou na `0x41` and pokud EDD je přítomné, pak `get_edd_info` znovu zavolá přerušení `0x13` , ale s hodnotu `ah` rovno `0x48` a `si` obsahující adresu buffru, kam se EDD informace uloží.

Závěr
--------------------------------------------------------------------------------

Toto je závěr druhé části o  vnitřnostech Linuxového jádra. V další části se podíváme na modesetting videa zbytek příprav před přechodem do protected módu a skokem do něj samotným.

Máte-li dotazy nebo připomínky, můžete mi napsat na [twitter](https://twitter.com/0xAX).

Links
--------------------------------------------------------------------------------

* [Protected mode](http://en.wikipedia.org/wiki/Protected_mode)
* [Protected mode](http://wiki.osdev.org/Protected_Mode)
* [Long mode](http://en.wikipedia.org/wiki/Long_mode)
* [Nice explanation of CPU Modes with code](http://www.codeproject.com/Articles/45788/The-Real-Protected-Long-mode-assembly-tutorial-for)
* [How to Use Expand Down Segments on Intel 386 and Later CPUs](http://www.sudleyplace.com/dpmione/expanddown.html)
* [earlyprintk documentation](https://github.com/torvalds/linux/blob/v4.16/Documentation/x86/earlyprintk.txt)
* [Kernel Parameters](https://github.com/torvalds/linux/blob/v4.16/Documentation/admin-guide/kernel-parameters.rst)
* [Serial console](https://github.com/torvalds/linux/blob/v4.16/Documentation/admin-guide/serial-console.rst)
* [Intel SpeedStep](http://en.wikipedia.org/wiki/SpeedStep)
* [APM](https://en.wikipedia.org/wiki/Advanced_Power_Management)
* [EDD specification](http://www.t13.org/documents/UploadedDocuments/docs2004/d1572r3-EDD3.pdf)
* [TLDP documentation for Linux Boot Process](http://www.tldp.org/HOWTO/Linux-i386-Boot-Code-HOWTO/setup.html) (old)
* [Previous Part](linux-bootstrap-1.md)
