# Zaváděcí proces jádra

Tato kapitola popisuje zaváděcí proces linuxového jádra. Následující seznam příspěvků popisuje celý cyklus načítání jádra: 

* [Od zavaděče k jádru](linux-bootstrap-1.md) - popisuje všechny etapy od zapnutí počítače ke spuštění první instrukce jádra.
* [První kroky v nastavení jádra](linux-bootstrap-2.md) - popisuje první kroky v nastavení jádra. Uvidíte zde inicializaci haldy, dotazy na různé paramatery jako například EDD, IST, atd.
* [Inicializace Video módu a přechod  do protected módu](linux-bootstrap-3.md) - popisuje inicializaci video módu describes a přechod do protected módu.
* [Přechod do 64-bitového módu](linux-bootstrap-4.md) - popisuje přípravu na přechod do 64-bitového módu a podrobnosti přechodu.
* [Komprese jádra](linux-bootstrap-5.md) - popisuje přípravu pro rozbalení jádra a podrobnosti rozbalení.
* [Namátkový výběr adresy jádra](linux-bootstrap-6.md) - popisuje náhodný výběr adresy Linuxového jádra.

Tato kapitola se shoduje s  `Linux kernel v4.17`.